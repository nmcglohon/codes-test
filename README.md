# **IMPORTANT NOTICE**
## THE CODES PROJECT HAS BEEN MOVED

https://github.com/codes-org/codes

As of June 21, 2019, the location of the repo has been moved to GitHub as part of the codebase's shift to its open source BSD License. All issues will be moved in some form to the new location followed by merge requests (if possible). I will do my best to keep the master branch of the two repositories up to date with each other. That being said, if there is any conflict, the GitHub repo should be taken as the truth.

Thank you,

Neil McGlohon

https://github.com/codes-org/codes



# CODES Discrete-event Simulation Framework

https://xgitlab.cels.anl.gov/codes/codes/wikis/home

Discrete event driven simulation of HPC system architectures and subsystems has emerged as a productive and cost-effective means to evaluating potential HPC designs, along with capabilities for executing simulations of extreme scale systems. The goal of the CODES project is to use highly parallel simulation to explore the design of exascale storage/network architectures and distributed data-intensive science facilities. 

Our simulations build upon the Rensselaer Optimistic Simulation System (ROSS), a discrete event simulation framework that allows simulations to be run in parallel, decreasing the simulation run time of massive simulations to hours. We are using ROSS to explore topics including large-scale storage systems, I/O workloads, HPC network fabrics, distributed science systems, and data-intensive computation environments.

The CODES project is a collaboration between the Mathematics and Computer Science department at Argonne National Laboratory and Rensselaer Polytechnic Institute. We collaborate with researchers at University of California at Davis to come up with novel methods for analysis and visualizations of large-scale event driven simulations. We also collaborate with Lawrence Livermore National Laboratory for modeling HPC interconnect systems.

Documentation can be found in the wiki: 
https://xgitlab.cels.anl.gov/codes/codes/wikis/home